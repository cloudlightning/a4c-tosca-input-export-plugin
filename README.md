# CloudLightning plugin 
The CloudLightning plugin adds a new interface in Alien4Cloud to facilitate the CloudLightning *Resource Discovery* process. 
This allows a user to create a topology based on abstract CloudLightning services which can then be replaced by a concrete implementation, based on the type of resource returned by the Self Organising Self Managing (SOSM) Framework for Resource Management. 
The interaction between this plugin and the SOSM system is made through the Service Decomposition Engine REST server, which should be running on the same host, on port 8080.

### Instalation
Change into the root directory and execute `mvn clean package`. 
Drop `target/cloudlightning-plugin-sample-1.3.3.2.zip` in the *Plugins* tab in Alien 4 Cloud. 
