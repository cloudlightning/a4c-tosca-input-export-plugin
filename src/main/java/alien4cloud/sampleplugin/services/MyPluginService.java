package alien4cloud.sampleplugin.services;

import alien4cloud.sampleplugin.topology.ClNodeTemplate;
import alien4cloud.sampleplugin.topology.TopologyTemplate;
import alien4cloud.topology.TopologyDTO;
import org.alien4cloud.tosca.model.templates.NodeTemplate;
import org.alien4cloud.tosca.model.templates.Topology;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Sample of a plugin service.
 */
@Service
public class MyPluginService {

    public TopologyTemplate buildClTopologyTemplate(TopologyDTO topologyDTO) {

        TopologyTemplate topologyTemplate=new TopologyTemplate();
        Topology topology=topologyDTO.getTopology();
        Map<String, NodeTemplate> nodeTemplateMap=topology.getNodeTemplates();


        for(Map.Entry<String, NodeTemplate> entry : nodeTemplateMap.entrySet()){

            ClNodeTemplate clNodeTemplate=new ClNodeTemplate();
            clNodeTemplate.setId(entry.getKey());
            clNodeTemplate.setType(entry.getValue().getType());
            topologyTemplate.addClNodeTemplate(clNodeTemplate);
        }
        return topologyTemplate ;
    }

    public String sayHelloTo() {
        return "hello teodora";
    }
}
