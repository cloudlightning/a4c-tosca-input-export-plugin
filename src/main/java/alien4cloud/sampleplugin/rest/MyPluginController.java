package alien4cloud.sampleplugin.rest;

import javax.annotation.Resource;

import alien4cloud.rest.utils.JsonUtil;
import alien4cloud.sampleplugin.topology.TopologyTemplate;
import alien4cloud.topology.TopologyDTO;
import alien4cloud.utils.jackson.JSonMapEntryArrayDeSerializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import alien4cloud.audit.annotation.Audit;
import alien4cloud.rest.model.RestResponse;
import alien4cloud.rest.model.RestResponseBuilder;
import alien4cloud.sampleplugin.services.MyPluginService;
import io.swagger.annotations.ApiOperation;

import java.io.IOException;


/**
 *
 * Simple REST controller provided by the plugin
 */
@Slf4j
@RestController
@RequestMapping("/rest/cloudlightning")
public class MyPluginController {
    @Resource
    private MyPluginService service;

    @ApiOperation(value = "Prepare Cloudlightning request document")
    @RequestMapping(value= "/requestdocument", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Audit
    private RestResponse<String> prepareDocument(@RequestBody String topologyResponseText) {



       // TopologyDTO topologyDTO =topologyResponse.getData();

        // log.info("----------Cl response="+payload);
       // return RestResponseBuilder.<String> builder().data(service.sayHelloTo()).build();
        return RestResponseBuilder.<String> builder().data("teodora").build();
    }
}