package alien4cloud.sampleplugin.topology;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by teodora on 31.08.2016.
 */
@Getter
@Setter
public class TopologyTemplate {

    private List<ClNodeTemplate> clNodeTemplates=new ArrayList<>();

    public void addClNodeTemplate(ClNodeTemplate clNodeTemplate){
        clNodeTemplates.add(clNodeTemplate);
    }

}
