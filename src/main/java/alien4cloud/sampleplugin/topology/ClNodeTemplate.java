package alien4cloud.sampleplugin.topology;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * Created by teodora on 31.08.2016.
 */
@Getter
@Setter
public class ClNodeTemplate {

    private String id;
    private String type;

   // private Map<String, AttributeTemplate> attributes;

}
