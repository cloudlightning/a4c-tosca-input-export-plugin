/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* global define */

'use strict';

define(function (require) {
    var modules = require('modules');

    modules.get('a4c-plugin-sde', ['ngResource']).factory('cloudlightningService', ['$resource',
        function($resource) {

            var clRequestDoc=$resource('rest/cloudlightning/requestdocument',{},{

                'prepare':{

                    method: 'POST',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                }

            });
            
            var clResponseDoc=$resource('http://:sdeEndpoint/sde/optimize',{},{

                'receive':{
                    method: 'POST',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                }
            }
                
            );

            var optimizationStatus=$resource('http://:sdeEndpoint/sde/optimize/:appName/:appVersion',{},{

                'receive':{
                    method: 'GET',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                },

                'delete':{
                    method: 'DELETE',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                }
            });

            
            var clComponents=$resource('rest/components/search',{},{
               'getComponents':{
                   method:'POST',
                   isArray:false,
                   headers:{
                       'Content-Type':'application/json; charset=UTF-8'
                   }
               } 
                
            });

            var clReplaceComponent=$resource('rest/latest/editor/:topologyId/execute',{},{
                'replace':{
                    method: 'POST',
                    isArray:false,
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }

                }
            });

            var saveModifications=$resource('rest/latest/editor/:topologyId',{},{
                'create':{
                    method: 'POST',
                    isArray:false,
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }

                }
            });



         
            return {
                'cloudlightningRequestDocument': clRequestDoc,
                'cloudlightningResponseDocument': clResponseDoc,
                'cloudlightningComponents':clComponents,
                'cloudlightningReplaceComponent':clReplaceComponent,
                'saveModifications':saveModifications,
                'optimizationStatus':optimizationStatus
            };
        }
    ]);
});