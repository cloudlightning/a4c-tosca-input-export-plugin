/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// this module is used to prefix the plugins url with the path added by alien 4 cloud plugin file server.


/* global define */

'use strict';

define(function (require) {
  // plugins module from alien 4 cloud
  var plugins = require('plugins');
  var pluginId = 'a4c-plugin-sde';
  return {
    enabled: true, // when runing with grunt serve the prefixing must be disabled
    prefix: function(url) {
      if(this.enabled) {
        return plugins.base(pluginId) + url;
      } else {
        return url;
      }
    }
  };
});
