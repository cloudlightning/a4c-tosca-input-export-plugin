/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Created by teodora on 06.09.2016.
 */

define(function (require) {
    'use strict';

    var modules = require('modules');
    var prefixer = require('scripts/plugin-url-prefixer');



    modules.get('a4c-plugin-sde').directive('clTopoChange',function(){
        return {
            template : "<p>Made by a directive!</p>"
        };
    }); // directive
});
