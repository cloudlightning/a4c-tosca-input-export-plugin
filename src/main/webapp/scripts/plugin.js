// This is the ui entry point for the plugin
/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global define */



'use strict';


function waitForMutation(parentNode, isMatchFunc, handlerFunc, observeSubtree, disconnectAfterMatch) {
  var defaultIfUndefined = function(val, defaultVal) {
    return (typeof val === "undefined") ? defaultVal : val;
  };

  observeSubtree = defaultIfUndefined(observeSubtree, false);
  disconnectAfterMatch = defaultIfUndefined(disconnectAfterMatch, false);

  var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      if (mutation.addedNodes) {
        for (var i = 0; i < mutation.addedNodes.length; i++) {
          var node = mutation.addedNodes[i];
          if (isMatchFunc(node)) {
            handlerFunc(node);
            if (disconnectAfterMatch) observer.disconnect();
          };
        }
      }
    });
  });

  observer.observe(parentNode, {
    childList: true,
    attributes: false,
    characterData: false,
    subtree: observeSubtree
  });
}


function buildQuery(filters){

    var query=new Object();

    //add empty query field
    query["query"]="";

    //add from and size fields
    query["from"]=0;
    query["size"]=200;

    //add type fields
    query["type"]="NODE_TYPE";

    //add filters


    query["filters"]=filters;

    return query;


}

function satisfies(performance, performanceString){
  var mapping = {"Low": 0.99, "Medium": 1.5, "Premium": 3}
  console.log(performance + " ? " + performanceString);
  return performance > mapping[performanceString];
}

function prepareClRequest(topologyJson, performanceString){

    var clFullRequest = {};
    var clRequest=new Array();


    if(topologyJson.topology!=undefined){

        var topology=topologyJson.topology;

        if(topology.nodeTemplates!=undefined){

            var nodeTemplates=topology.nodeTemplates;
            clFullRequest["name"] = topology.archiveName;
            clFullRequest["version"] = topology.archiveVersion;
            for (var key in nodeTemplates){

                var isAbstractNode=false;


                var nodeName = key;
                var nodeValue = nodeTemplates[key];


                var clNode=new Object();
                clNode.name=nodeName;


                if(nodeValue.type!=undefined) {
                    clNode.type = nodeValue.type;
                    var capability=null;

                    if (topologyJson.nodeTypes!=undefined && topologyJson.nodeTypes[clNode.type]!=undefined && topologyJson.nodeTypes[clNode.type].abstract!=undefined && topologyJson.nodeTypes[clNode.type].abstract==true )
                    {

                        isAbstractNode=true;


                        console.log(nodeName+" is abstract");


                        //get service capability
                        if(nodeValue.capabilities!=undefined){
                            for(var i = 0; i < nodeValue.capabilities.length; i++) {
                                if (nodeValue.capabilities[i].key != undefined && nodeValue.capabilities[i].key != null &&  nodeValue.capabilities[i].key==="service" ) {
                                        if(nodeValue.capabilities[i].value.type != undefined && nodeValue.capabilities[i].value.type != null){

                                            capability=nodeValue.capabilities[i].value.type;
                                            break;
                                        }
                                }
                            }
                        }


                        //get non-abstract nodes with the same capability

                        //build query using current node type
                        if(capability!=null) {
                            var filters = {"abstract": [false], "capabilities.type": [capability]}
                            var queryForComponents = buildQuery(filters);

                            //console.log("Query for component "+JSON.stringify(queryForComponents));

                            $.ajax({
                                url: 'rest/latest/components/search',
                                type: 'post',
                                async: false,
                                dataType: 'json',
                                contentType: "application/json",
                                success: function (result) {
                                    // console.log("Received data:from component search:"+JSON.stringify(result.data.data));
                                    var impList=new Array();
                                    for(var i=0;i<result.data.data.length;i++){
                                        var service = result.data.data[i];
                                        //check if service satisfies performance 
                                        var performance = 0.0;
                                        for(var j=0; j < service.tags.length; j++){
                                          if(service.tags[j].name === "performance"){
                                            performance = service.tags[j].value;
                                            console.log("performance found " + performance);
                                          }
                                        }

                                        if(satisfies(performance, performanceString)){
                                          var imp=new Object();
                                          imp['elementId']=service.elementId;
                                          imp['id']=service.id;
                                          imp['archiveVersion']=service.archiveVersion;
                                          impList.push(imp);
                                        }else{
                                          console.log("Filtering out " + service.elementId + " because performance " + performance + " < " + performanceString);
                                        }
                                    }
                                    if(impList.length < 1){
                                      //if no service satisfies the performance, then add all of them and let SOSM choose
                                      console.log("No service was satisfying performance constraint. Adding all and letting SOSM choose");
                                      for(var i=0;i<result.data.data.length;i++){
                                        var service = result.data.data[i];
                                        var imp=new Object();
                                        imp['elementId']=service.elementId;
                                        imp['id']=service.id;
                                        imp['archiveVersion']=service.archiveVersion;
                                        impList.push(imp);
                                      }
                                    }
                                    clNode.implementationList=impList;

                                },
                                data: JSON.stringify(queryForComponents)
                            });
                        }

                    }else{
                      if(nodeValue.capabilities!=undefined){
                            for(var i = 0; i < nodeValue.capabilities.length; i++) {
                                if (nodeValue.capabilities[i].key != undefined && nodeValue.capabilities[i].key != null &&  nodeValue.capabilities[i].key==="service" ) {
                                        if(nodeValue.capabilities[i].value.type != undefined && nodeValue.capabilities[i].value.type != null){

                                            capability=nodeValue.capabilities[i].value.type;
                                            break;
                                        }
                                }
                            }


                        }
                    }

                    if (topologyJson.nodeTypes!=undefined && topologyJson.nodeTypes[clNode.type]!=undefined && topologyJson.nodeTypes[clNode.type].id!=undefined )
                    {
                        clNode.id=topologyJson.nodeTypes[clNode.type].id;
                    }

                }


                if(isAbstractNode)
                     clRequest.push(clNode);
                else if(capability != null){
                  var impList=new Array();
                  var imp=new Object();
                  imp['elementId']=topologyJson.nodeTypes[clNode.type].elementId;
                  imp['id']=topologyJson.nodeTypes[clNode.type].id;
                  imp['archiveVersion']=topologyJson.nodeTypes[clNode.type].archiveVersion;
                  impList.push(imp);
                  clNode.implementationList = impList;
                  clRequest.push(clNode);

                }
            }
        }
    }
    clFullRequest["services"] = clRequest;
    return clFullRequest;

}
function getNodeTemplatesList(topologyJson){
    var nodeTemplatesList=new Array();


    // console.log("Topology in getNodeTemplatelist:"+JSON.stringify(topologyJson));

    if(topologyJson.topology!=undefined) {

        var topology = topologyJson.topology;

        if (topology.nodeTemplates != undefined) {

            var nodeTemplates = topology.nodeTemplates;
            for (var key in nodeTemplates) {

                var nodeName = key;
                var nodeValue = nodeTemplates[key];

                var clNode = new Object();
                clNode.id = nodeName;

                if (nodeValue.type != undefined) {
                    clNode.type = nodeValue.type;
                }

                nodeTemplatesList.push(clNode);
            }
        }
    }
    return nodeTemplatesList;
}




define(function (require) {
  var states = require('states');
  var modules = require('modules');
  var prefixer = require('scripts/plugin-url-prefixer');

  require('scripts/cloudlightning-service.js');


  require('scripts/cl-yaml.js');
  require('scripts/cl-deploy.js');
  require('scripts/cl-optimize.js');
  require('scripts/cloudlightning.js');
  require('scripts/directives/clTopoChange.js');
 

  //console.log("plugin added");
    modules.get("uiSwitch",[]).directive("switch",function(){return{restrict:"AE",replace:!0,transclude:!0,template:function(n,e){var s="";return s+="<span",s+=' class="switch'+(e.class?" "+e.class:"")+'"',s+=e.ngModel?' ng-click="'+e.ngModel+"=!"+e.ngModel+(e.ngChange?"; "+e.ngChange+'()"':'"'):"",s+=' ng-class="{ checked:'+e.ngModel+' }"',s+=">",s+="<small></small>",s+='<input type="checkbox"',s+=e.id?' id="'+e.id+'"':"",s+=e.name?' name="'+e.name+'"':"",s+=e.ngModel?' ng-model="'+e.ngModel+'"':"",s+=' style="display:none" />',s+='<span class="switch-text">',s+=e.on?'<span class="on">'+e.on+"</span>":"",s+=e.off?'<span class="off">'+e.off+"</span>":" ",s+="</span>"}}});

    modules.get('a4c-plugin-sde',['a4c-applications','a4c-topology-editor','ui.bootstrap', 'ui.select']).controller('HelloController', ['$scope', '$location', 'topologyServices','clTopoEditor','cloudlightningService',
        function($scope,$location,topologyServices,clTopoEditor,cloudlightningService) {


            clTopoEditor($scope);
            $scope.clTopologyStatus="NOT_STARTED";
            $scope.clPrintingStatus="NOT STARTED";
            $scope.autodeploy = false;
            $scope.record = false;
            $scope.repeat = 0;
            if(!$scope.sdeEndpoint){

              $scope.sdeEndpoint=$location.host().split(":")[0] + ":8080";
            }
            console.log($scope.topologyId);
            var appDetails = $scope.topologyId.split(":");




            $scope.oldNodes=[];
            $scope.newNodes=[];
            $scope.replacements=[];
            $scope.finishedChanges=false;
            $scope.performance = ["Low", "Medium", "Premium"];
            $scope.selectedPerformance = {item: $scope.performance[0]};

            cloudlightningService.optimizationStatus.receive({appName: appDetails[0], appVersion: appDetails[1], sdeEndpoint: $scope.sdeEndpoint},
               function (optimization) {
                  
                  if(optimization.status == "SUCCESS"){

                    $scope.clTopologyStatus="OPTIMISED";
                    $scope.clPrintingStatus="OPTIMISED";
                    for(var i in optimization.requestList){
                      $scope.oldNodes.push(optimization.requestList[i].name);
                    }
                    for(var i in optimization.responseList){
                      $scope.newNodes.push(optimization.responseList[i].type.split(":")[0]);
                    }
                    $scope.finishedChanges=true;
                    
                  }else if(optimization.status == "PROCESSING"){
                    $scope.clTopologyStatus="IN_PROGRESS";
                    $scope.clPrintingStatus="IN PROGRESS";

                  }else if(optimization.status == "FAIL"){
                    $scope.clTopologyStatus="ERROR";
                    $scope.clPrintingStatus=optimization.status;
                  }

              });
            


            $scope.cloudlightningTopo=function() {
                $scope.oldNodes=[];
                $scope.newNodes=[];
                $scope.replacements=[];
                $scope.clTopologyStatus = "IN_PROGRESS";
                // console.log("topo status=" + $scope.clTopologyStatus);
                $scope.clPrintingStatus = "IN PROGRESS";

                topologyServices.dao.get({
                    topologyId: $scope.topologyId
                }, function (successResult) {

                    // console.log("Topology:" + JSON.stringify(successResult));

                    var clRequest = prepareClRequest(successResult.data, $scope.selectedPerformance.item);
                    // console.log("Cl Request with impl new:"+JSON.stringify(clRequest));


                    var nodeTemplatesList = getNodeTemplatesList(successResult.data);

                    // console.log("NodeTemplatesList:"+JSON.stringify(nodeTemplatesList));

                    cloudlightningService.cloudlightningResponseDocument.receive({sdeEndpoint: $scope.sdeEndpoint},
                        clRequest, function (clResponse) {
                            $scope.topologyJson = clResponse;

                            // console.log("ClResponse:");
                            // console.log(clResponse);



                            $scope.clTopo.editClTopo(clRequest["services"], clResponse["responseList"], nodeTemplatesList, 0, 0,null, 
                                  function (prevOpId){
                                      $scope.clTopo.applyAdditionalRelationships(clResponse["additionalOperations"], 0, prevOpId, $scope.autodeploy, clResponse["applicationName"] + ":" + clResponse["applicationVersion"], clResponse["environmentId"]);
                                  });


                        });




                });
            }


            $scope.cloudlightningTopoRedirect=function () {

                var fullPath=$location.path();
                var entities=fullPath.split("/");
                $location.path("/applications/details/"+entities[entities.length-2]+"/topology/editor");

            }

            $scope.cloudlightningTopoRelease=function () {
                $scope.oldNodes=[];
                $scope.newNodes=[];
                $scope.replacements=[];
                cloudlightningService.optimizationStatus.delete({appName: appDetails[0], appVersion: appDetails[1], sdeEndpoint: $scope.sdeEndpoint},
                        function (clResponse) {
                          if(clResponse["response"]){
                            $scope.topologyJson = clResponse;

                            // console.log("ClResponse:");
                            // console.log(clResponse);

                            $scope.clTopo.editClTopo(clResponse["services"], clResponse["response"], null, 0, 0,null, 
                              function (prevOpId) {
                                $scope.clTopo.deleteNodes(clResponse["toDelete"], 0, prevOpId, $scope.autodeploy);
                              });
                          }else{
                            //recovering from failed resource discovery -> deleting previous request.
                            $scope.clTopologyStatus="NOT_STARTED";
                            $scope.clPrintingStatus="NOT STARTED";
                          }
                        });


            }

            $scope.setSdeEndpoint=function (endpoint) {

                $scope.sdeEndpoint = endpoint;

            }

            $scope.setSelectedPerformance=function (perf) {

                $scope.selectedPerformance = perf;

            }





        }
    ]);

  var templateUrl = prefixer.prefix('views/hello.html');

  // register plugin state
  states.state('applications.detail.a4cpluginsample', {
    url: '/cloudlightning',
    templateUrl: templateUrl,
    controller: 'HelloController'

  });





  //console.log("after controller declaration");
  waitForMutation(

      document.body,
      function(node) {
        if ( node && node.nodeType !== Node.ELEMENT_NODE ){
          return false;
        }
       // if(node.querySelector("#btn-app-topology-plan")!=null || node.querySelector('#btn-deploy')!=null)
        if(node.querySelector("#btn-app-topology-plan")!=null || (document.getElementById('am.applications.detail.deployment')!=null && !document.getElementById('clOptimization')))
            return true;
        return false;

      },

      function(node) {
        // console.log("node found");
     //   alert("found");
        if(document.getElementById('btn-app-topology-plan')) {
          if (!document.getElementById("btn-app-topology-cl")) {
           // alert("found empty view");

            //insert element

          /*  var $cl_button = $('<a class="btn btn-default btn-xs" onclick="clYamlDisplay()" id="btn-app-topology-cl" >CL yaml</a>');

            $(document.getElementById('btn-app-topology-plan')).after($cl_button);

            angular.element(document).injector().invoke(function ($compile) {
              var scope = angular.element($cl_button).scope();
              $compile($cl_button)(scope);
            });
            */

          } else {


            ///alert("found view with element");
          }
        }else {
          // console.log("found the other type of node2");
          if(document.getElementById('am.applications.detail.deployment')){
            
            if(!document.getElementById('clOptimization')) {

              var parentNode = document.getElementById('am.applications.detail.topology').parentNode;

              var hrefLink=document.getElementById('am.applications.detail.deployment').getAttribute("href");
              // console.log("hrefLink="+hrefLink);
              hrefLink=hrefLink.substring(0,hrefLink.lastIndexOf("/")+1);
              
              var $cl_optimize = $('<li id="clOptimization"><a href="'+hrefLink+'cloudlightning" tooltip="Resource Discovery" tooltip-placement="right" tooltip-append-to-body="true" ><i class="fa fa-bolt" aria-hidden="true"></i></a></li>');
              $(parentNode).after($cl_optimize);

              // console.log("here4");

              /*angular.element(document).injector().invoke(function ($compile,$rootScope) {
                  $rootScope.$apply(function() {
                    var scope = angular.element($cl_optimize).scope();

                    $compile($cl_optimize)(scope);
                  });
              });*/


              // console.log("insert cloudlightning button4 before" + parentNode.tagName);
            }


          }
        } /*if(document.getElementById('btn-deploy')){
          if (!document.getElementById("btn-deploy-cl")) {
           // alert("found empty deploy");

            //insert element

            var $cl_button_deploy = $('<button id="btn-deploy-cl" type="button" style=" margin-left: 30px" class="btn btn-info application-deploy-button" onclick="clDeploy()"><i class="fa fa-play" ></i> Deploy CL</button>');
            $(document.getElementById('btn-deploy')).after($cl_button_deploy);

            angular.element(document).injector().invoke(function ($compile) {
              var scope = angular.element($cl_button_deploy).scope();
              $compile($cl_button_deploy)(scope);
            });



          } else {


            //alert("found deploy with element");
          }
        }*/
      },
      true, false );











});

