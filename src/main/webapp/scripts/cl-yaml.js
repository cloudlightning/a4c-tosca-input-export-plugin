/*
 * Copyright 2017 Institute e-Austria Timisoara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Created by teodora on 03.08.2016.
 */
function clYamlDisplay(){

    console.log("request to get cloudlightning yaml");

    //get topologyId from URL
    var url=window.location.href ;
    var res=url.split("/");
    var topologyId=res[res.length-2]

    var scope = angular.element(document.getElementById("topology-editor")).scope();
    //get yaml
    getSimpleYaml(scope.topologyId);
    console.log("scope="+scope.topologyId);
    console.log("url="+res[res.length-2]);
}

function getSimpleYaml(topologyId){

  
    $.ajax({
        url:'/rest/topologies/'+topologyId+'/yaml',
        method:'GET',
        dataType:'json',
        success:function(data){
            console.log("received yaml="+data);
            getClYaml(data);
        }

    });

   
    console.log("finished ajax request");
    
}

function getClYaml(yaml){

  

    yamlJson=JSON.stringify(yaml);

    yamlJson=yamlJson.replace(/\\n/g, "\\\\n");

    $.ajax({
       url:'http://localhost:8080/cloudlightning/yaml',
       method: 'POST',
       data: yamlJson,
       dataType: 'json',
        contentType: 'application/json',
        success:function(data){
              dataJson=JSON.stringify(data);
             dataJson=dataJson.replace(/\\\\n/g,"\\n");

            showYaml(JSON.parse(dataJson));
        },
        error: function (request, status, error) {
            console.log("ajax error="+request.responseText);
        }

        
    });
}

function showYaml(content){
    
    console.log("show yaml");

    var scope = angular.element(document.getElementById("topology-editor")).scope();

    scope.$apply(function() {
        scope.view = 'YAML';
        scope.yaml.content=content.data;
    });

    console.log("displayed yaml");

    
}
